#!/usr/bin/env bats

teardown () {
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "check KernelHasNoDeadlock state is communicated" {

  NODE=`kubectl get no --no-headers | head -n 1 | awk '{print $1}'`
  run kubectl describe node ${NODE}
  [ $status -eq 0 ]
  [[ "$output" =~ KernelHasNoDeadlock ]]

}
